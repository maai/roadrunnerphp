-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2015 at 08:16 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

drop DATABASE IF EXISTS `roadrunnerdb`

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `roadrunnerdb`
--
CREATE DATABASE IF NOT EXISTS `roadrunnerdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `roadrunnerdb`;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `addressID` int(11) NOT NULL AUTO_INCREMENT,
  `addressLine` varchar(100) NOT NULL,
  `city` int(11) NOT NULL,
  `postalcode` varchar(7) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  PRIMARY KEY (`addressID`),
  UNIQUE KEY `unique_addressID` (`addressID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`addressID`, `addressLine`, `city`, `postalcode`, `lat`, `lng`) VALUES
(76, '1836 Rue Ste-Catherine O', 1, 'H3H 1M1', 45.4931385, -73.5798272),
(77, '305 Rue Ste-Catherine O', 1, ' H2X 2A', 45.5067288, -73.5668239),
(78, '3650 Rue McTavish', 1, 'H3A 1Y2', 45.50423, -73.579699),
(79, '4743 Chemin Queen Mary', 1, 'H3W 1W8', 45.4889923, -73.6248567),
(80, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931),
(81, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931),
(82, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931),
(83, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931),
(84, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931),
(85, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931),
(86, '6585 Rue Clark', 1, 'H2S 3E8', 45.5308877, -73.6113931);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `cityID` int(11) NOT NULL,
  `cityName` varchar(50) NOT NULL,
  `city_Province` int(2) NOT NULL,
  PRIMARY KEY (`cityID`),
  UNIQUE KEY `unique_cityID` (`cityID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`cityID`, `cityName`, `city_Province`) VALUES
(1, 'Montreal', 1),
(2, 'Laval', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countrieslist`
--

CREATE TABLE IF NOT EXISTS `countrieslist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `countrieslist`
--

INSERT INTO `countrieslist` (`id`, `country_code`, `country_name`) VALUES
(2, 'CA', 'Canada');

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `provinceID` int(2) NOT NULL,
  `provinces_Name` varchar(50) NOT NULL,
  `provinces_country` int(11) NOT NULL,
  PRIMARY KEY (`provinceID`),
  UNIQUE KEY `unique_provinceID` (`provinceID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`provinceID`, `provinces_Name`, `provinces_country`) VALUES
(1, 'Quebec', 2);

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE IF NOT EXISTS `trip` (
  `tripId` int(11) NOT NULL AUTO_INCREMENT,
  `clt_name` text NOT NULL,
  `clt_Phone_Number` varchar(10) NOT NULL,
  `dateOrder` datetime NOT NULL,
  `datePickUp` datetime DEFAULT NULL,
  `dateDelevery` datetime DEFAULT NULL,
  `start_address` int(11) NOT NULL,
  `destination_address` int(11) DEFAULT NULL,
  `taxi` int(11) DEFAULT NULL,
  `intAgent` int(11) NOT NULL,
  `taxiStartingLan` double DEFAULT NULL,
  `taxiStartingLng` double DEFAULT NULL,
  `tripLength` int(11) DEFAULT NULL,
  `tripTime` int(11) DEFAULT NULL,
  PRIMARY KEY (`tripId`),
  UNIQUE KEY `unique_tripId` (`tripId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`tripId`, `clt_name`, `clt_Phone_Number`, `dateOrder`, `datePickUp`, `dateDelevery`, `start_address`, `destination_address`, `taxi`, `intAgent`, `taxiStartingLan`, `taxiStartingLng`, `tripLength`, `tripTime`) VALUES
(38, 'anass', '1234567890', '2015-03-31 02:44:05', '2015-03-31 02:49:55', '2015-03-31 03:10:55', 76, 86, 5, 1, NULL, NULL, 6701, 751),
(39, 'Sam', '3214569870', '2015-03-31 02:16:45', NULL, NULL, 77, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(40, 'Ronald', '514321456', '2015-03-31 02:17:44', NULL, NULL, 78, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(41, 'walker red', '450659823', '2015-03-31 02:21:55', NULL, NULL, 79, NULL, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userLogin` varchar(16) NOT NULL,
  `userPW` varchar(16) NOT NULL,
  `userFirstName` varchar(50) NOT NULL,
  `userLastName` varchar(50) NOT NULL,
  `userType` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `unique_userID` (`userID`),
  UNIQUE KEY `unique_userLogin` (`userLogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userID`, `userLogin`, `userPW`, `userFirstName`, `userLastName`, `userType`, `lat`, `lng`, `status`) VALUES
(1, 'admin', 'test', 'admin', '01', 1, NULL, NULL, NULL),
(5, 'taxi', 'test', 'taxi', '01', 2, 45.7055996, -73.4753011, 1),
(8, 'taxi2', 'test', 'taxi', '#02', 2, 45.4146906, -73.9488652, 0),
(9, 'taxi3', 'test', 'taxi', '#03', 2, 45.7055996, -73.4753011, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
