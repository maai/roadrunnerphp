<?php namespace controllers;
use core\view,
    \helpers\session,
    \helpers\password,
    \helpers\url,
    helpers\an_paginator,
    models\Address_model,
    models\trip_model;

class agent extends \core\controller{

    private $_address_model;
    private $_trip_model;
    private $_user_model;

	/**
	 * Call the parent construct
	 */
	public function __construct(){
		parent::__construct();
	}

	/**
	 * Define Index page title and load template files
	 */
	public function index() {
        //if not logged in go to login page
        if (session::get('logedIn')!=true){
            url::redirect('login');
        }

        $this->language->load('agent');
		$data['title'] = $this->language->get('agent_main_title');

		View::rendertemplate('header', $data);
		View::render('agent/main', $data);
		View::rendertemplate('footer', $data);
	}

	/**
	 * display the new trip
	 */
	public function newtrip() {
        //if not logged in go to login page
        if (session::get('logedIn')!=true){
            url::redirect('login');
        }
        $this->language->load('trip');

		$data['lbltitle']       = $this->language->get('title');
        $data['lblclt_name']    = $this->language->get('clt_name');
        $data['lblclt_Phone']   = $this->language->get('clt_Phone');
        $data['lblstarting_adress'] =  $this->language->get('Address de depart');
        $data['lblpostal_code'] =  $this->language->get('Code postal');
        $data['lblcity'] =  $this->language->get('Ville');
        $data['lblcontry'] =  $this->language->get('Pays');
        $data['lblprovince'] =  $this->language->get('Province');

        $data['bt_submit'] =  $this->language->get('bt_submit');
        $data['bt_rest']=  $this->language->get('bt_rest');
        $data['bt_cancel'] =  $this->language->get('bt_cancel');

        $address= new Address_model();
        $data['contryList']   =  "Canada";
        $data['provinceList'] =  "Quebec";
        $data['cityList']     =  $address->getCitiesListByProvinces(1);       // get citys of Quebec

		//View::rendertemplate('header', $data);
		View::render('agent/newtrip', $data);

	}

    public function addTrip($loginerror=null) {
        $this->language->load('agent');
        $data['title'] = $this->language->get('agent_main_title');

        //if not logged in go to login page
        if (session::get('logedIn')!=true){
            url::redirect('login');
        }

        // If not logged yet
        if(isset($_POST['submit'])){

            $name=$_POST['txtCltName'];
            $clt_Phone_Number=$_POST['txtCltPhone'];
            $date = date("Y-m-d h:i:s");
            $addressLine=$_POST['txtStartingAdress'];
            $pc=$_POST['txtCP'];
            $city=$_POST['cityList'];
            $user=session::get('user');

            //convert the address to (lan,lng) then insert the address in the data base
            $posURL = "https://maps.googleapis.com/maps/api/geocode/xml?address=$addressLine, $city, QC $pc&key=".GOOGLE_KEY;
            $xml=simplexml_load_file($posURL) or die("Error: Cannot create object");
            $newAddress = array(
                'addressLine'            => $addressLine,
                'postalcode'             => $pc,
                'city'                   => $city,
                'lat'                    => $xml->result->geometry->location->lat,
                'lng'                    => $xml->result->geometry->location->lng
            );

            $this->_address_model= new \models\Address_model();
            $AddressID = $this->_address_model->insertNewAddress($newAddress);


         $StartingAddressData = $this->_address_model->getAddress($AddressID);
            //creat the trip and link the start address to the trip
            $cltData = array(
                'clt_name'              => $name,
                'clt_Phone_Number'      => $clt_Phone_Number,
                'dateOrder'             => $date,
                'start_address'         => $StartingAddressData[0]->addressID ,
                'intAgent'              => $user[0]->userID
            );

            $this->_trip_model= new \models\trip_model();
            $tripId = $this->_trip_model->insertNewTrip($cltData);
            $trip = $this->_trip_model->getTrip($tripId);
            $data['trip'] =$trip[0];

        }else{
            $data['trip'] =null;
        }
        $data['lbltitle']       = $this->language->get('title');
        $data['lblclt_name']    = $this->language->get('clt_name');
        $data['lblclt_Phone']   = $this->language->get('clt_Phone');
        $data['lblstarting_adress'] =  $this->language->get('Address de depart');
        $data['lblpostal_code'] =  $this->language->get('Code postal');
        $data['lblcity'] =  $this->language->get('Ville');
        $data['date'] =  $this->language->get('date');
        View::rendertemplate('header', $data);
        View::render('agent/addTripSuccess', $data);
        View::rendertemplate('footer', $data);
    }

    public function taxisTracking($p=0,$error=null){
        //if not logged in go to login page
        if (session::get('logedIn')!=true){
            url::redirect('login');
        }
        $this->language->load('agent');
        $NEXT       =$this->language->get('next');
        $PREVIOUS   =$this->language->get('previous');

        // setting up the pagination :
        $url=DIR.'agent/taxiTracking';
        $nbr_Rows_To_Display='2';
        $this->_user_model= new \models\User_model();
        $TaxiCount=$this->_user_model->getTaxiListCount();
        $pages = new \helpers\an_paginator($url,$nbr_Rows_To_Display,$p);
        $pages->set_total( $TaxiCount);
        $pages->set_Nav_Tag($NEXT,$PREVIOUS);

        $data['users'] =$this->_user_model->getTaxiList($pages->get_limit());
        $data['page_links']= $pages->page_links();
        $data['lbltitle']       = $this->language->get('title_taxi_list');
        $data['lblclt_name']    = $this->language->get('clt_name');
        $data['lblclt_Latitude']   = $this->language->get('latitude');
        $data['lblclt_Longitude']   = $this->language->get('longitude');

        View::rendertemplate('header', $data);
        View::render('agent/taxiTracking', $data);
        View::rendertemplate('footer', $data);

    }

    public function taxisTrackingXml($error=null){
        //if not logged in go to login page
        if (session::get('logedIn')!=true){
            url::redirect('login');
        }

        $this->_user_model= new \models\User_model();
        $data['tripStat']=$this->_user_model->getTaxiList();
        View::rendertemplate('header', $data);
        View::render('agent/stat', $data);
        View::rendertemplate('footer', $data);
    }
}
