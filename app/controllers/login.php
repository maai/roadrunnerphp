<?php namespace controllers;
use core\view,
    \helpers\session,
    \helpers\password,
    \helpers\url;

/*
 * login controller
 *
 * part of RoadRunner app, realised as activity educational purpose in the course Log330
 *
 * @author Anass Maai <anass.maai@gmail.com>,
 *         Sam Bellerose <samuel.bellerose1991@gmail.com>
 *         jonathan.stcyr@gmail.com,
 *         David Patry <zeldavslink@gmail.com>,
 *         Vincent Leclerc <vincent.leclerc.2@etsmtl.net>
 *
 *
 * @version 1.0
 * @date Date: 05/03/15
 * @Time: 10:51 PM
 */

/*
 * entry point to the system
 * type : controller
 * */

class login extends \core\controller{
    private $_userModel;

    /**
     * Call the parent construct
     */
    public function __construct(){
        parent::__construct();
        $this->language->load('login');
        $this->_userModel= new \models\User_model();
    }

    /**
     * Define main page title and load all template files
     */
    public function index($errMsg=null) {
        $this->login();
    }

    /**
     * Login action
     */
    public function login($loginerror=null) {

        //if logged ingo to admin front page
        if (session::get('logedIn')==true){
            $this->routeUser();
        }
        // If not logged yet
        if(isset($_POST['submit'])){
            $login=$_POST['login'];
            $password=$_POST['password'];
            $user=$this->_userModel->getUser($login,$password);
            if ($user!=null){
               session::set('logedIn', true);
               session::set('user', $user);

                if ($user[0]->userType=="1"){
                    url::redirect("agent");
                }elseif($user[0]->userType=="2"){
                    url::redirect("taxi");
                }
                echo session::get('logedIn');
            }else{
                $errMsg ='<p class="bg-warning">'. $this->language->get('connection_error_message') .'</p>';
                $data['error']=$errMsg;
            }
        }
        $data['title'] = $this->language->get('title');
        $data['login'] = $this->language->get('login');
        $data['password'] = $this->language->get('password');
        $data['bt_submit'] = $this->language->get('bt_submit');
        $data['bt_rest'] = $this->language->get('bt_rest');


        View::rendertemplate('header', $data);
        View::render('login/login', $data);
        View::rendertemplate('footer', $data);
    }

    /**
     * in case of errors
     */
    public function connectionError() {
        $data['title'] = $this->language->get('titleError');
        $data['connection_error_message'] = $this->language->get('connection_error_message');

        $data['title'] = $this->language->get('title');
        $data['login'] = $this->language->get('login');
        $data['password'] = $this->language->get('password');
        View::rendertemplate('header', $data);
        View::render('login/login', $data);
        View::rendertemplate('footer', $data);
    }

    public function logout() {
        echo (session::display());
        session::destroy();
        header( "Location:".DIR);
    }


    public function routeUser() {
        $user=session::get('user');
        switch ($user[0]->userType) {
            case 1:
                url::redirect('agent');
                break;
            case 2:
                url::redirect('taxi');
                break;
            default:
                url::redirect('main');
                break;
        }
    }

} 