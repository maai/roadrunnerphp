<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 06/03/15
 * Time: 11:24 AM
 */

namespace controllers;

use core\view,
    \helpers\session,
    \helpers\password,
    helpers\an_paginator,
    \helpers\url;
use models\trip_model;

class taxidriver extends \core\controller
{
    private $_trip_model;

    /**
     * Call the parent construct
     */
    public function __construct()
    {
        parent::__construct();
        //if not logged in , go to login page
        if (!session::get('logedIn')) {
            Url::redirect("login");
        }
        $this->language->load('login');
    }

    public function index($lat = 0, $lng = 0)
    {
        $data['lbltitle'] = $this->language->get('title');
        View::rendertemplate('header', $data);
        View::render('taxidriver/main', $data);
        View::rendertemplate('footer', $data);
    }

    public function availableTrips($lat, $lng, $p = 0)
    {
        //if not logged in go to login page
        if (session::get('logedIn') != true) {
            url::redirect('login');
        }

        $data['tacxiLat'] = $lat;
        $data['tacxiLng'] = $lng;
        $distance = 2;
        $this->_trip_model = new \models\trip_model();
        /*pagination*/
        $this->language->load('agent');
        $NEXT = $this->language->get('next');
        $PREVIOUS = $this->language->get('previous');
        $url = DIR . 'taxi/availableTrips';
        $nbr_Rows_To_Display = '5';

        $availibalTripCount = $TaxiCount = count($this->_trip_model->selectAddressWithin($lat, $lng, $distance, ''));

        $pages = new \helpers\an_paginator($url, $nbr_Rows_To_Display, $p);
        $pages->set_total($availibalTripCount);
        $pages->set_Nav_Tag($NEXT, $PREVIOUS);
        $data['page_links'] = $pages->page_links();

        /* end pagination */
        $data['tripList'] = $this->_trip_model->selectAddressWithin($lat, $lng, $distance, $pages->get_limit());

        View::render('taxidriver/cltList', $data);


    }

    public function selectTrip($tripID = 0)
    {
        //if not logged in go to login page
        if (session::get('logedIn') != true) {
            url::redirect('login');
        }

        $this->language->load('trip');
        $data['lbltitle'] = $this->language->get('title_detail_trip');
        $data['lblclt_name'] = $this->language->get('clt_name');
        $data['lblclt_Phone'] = $this->language->get('clt_Phone');
        $data['lblstarting_adress'] = $this->language->get('Address de depart');
        $data['lblpostal_code'] = $this->language->get('Code postal');
        $data['lblcity'] = $this->language->get('Ville');
        $data['bt_submit'] = $this->language->get('bt_submit');
        $data['bt_rest'] = $this->language->get('bt_rest');
        $data['bt_cancel'] = $this->language->get('bt_cancel');

        $address = new \models\Address_model();
        $trip = new \models\trip_model();

        $selectedTrip = $trip->getTrip($tripID);
        $data['address'] = $address->getAddress($selectedTrip[0]->start_address);
        $data['trip'] = $selectedTrip;

        //$data['cityList']     =  $address->getCitiesListByProvinces(1);       // get citys of Quebec
        //

        View::rendertemplate('header', $data);
        View::render('taxidriver/tripDetails', $data);
        View::rendertemplate('footer', $data);

    }

    public function ignoreClt()
    {
        echo("ignoreClt()");
    }

    public function updateTrip($tripId = 0)
    {
        echo("updateATrip($tripId)");
        //if not logged in go to login page
        if (session::get('logedIn') != true) {
            url::redirect('login');
        }

        $this->language->load('trip');
        $data['lbltitle'] = $this->language->get('title_trip_distination');
        $data['lblclt_name'] = $this->language->get('clt_name');
        $data['lblclt_Phone'] = $this->language->get('clt_Phone');
        $data['lblstarting_adress'] = $this->language->get('Address de destination');
        $data['lblpostal_code'] = $this->language->get('Code postal');
        $data['lblcity'] = $this->language->get('Ville');
        $data['bt_submit'] = $this->language->get('bt_submit');
        $data['bt_rest'] = $this->language->get('bt_rest');
        $data['bt_cancel'] = $this->language->get('bt_cancel');

        $address = new \models\Address_model();
        $trip = new \models\trip_model();

        $selectedTrip = $trip->getTrip($tripId);

        $data['trip'] = $selectedTrip;
        $data['cityList'] = $address->getCitiesListByProvinces(1); // get citys of Quebec
        View::rendertemplate('header', $data);
        View::render('taxidriver/updateTripForm', $data);
        View::rendertemplate('footer', $data);

    }

    public function saveTrip()
    {

        if (isset($_POST['submit'])) {
            $tripID = $_POST['tripID'];
            $addressLine = $_POST['DestinationAdress'];
            $pc = $_POST['txtCP'];
            $city = $_POST['cityList'];
            $user = session::get('user');

            //convert the address to (lan,lng) then insert the address in the data base
            $posURL = "https://maps.googleapis.com/maps/api/geocode/xml?address=$addressLine, $city, QC $pc&key=" . GOOGLE_KEY;
            $xml = simplexml_load_file($posURL) or die("Error: Cannot create object");
            $newAddress = array(
                'addressLine' => $addressLine,
                'postalcode' => $pc,
                'city' => $city,
                'lat' => $xml->result->geometry->location->lat,
                'lng' => $xml->result->geometry->location->lng
            );

            $this->_address_model = new \models\Address_model();
            $AddressID = $this->_address_model->insertNewAddress($newAddress);

            $destinationAddressData = $this->_address_model->getAddress($AddressID);

            $date = date("Y-m-d h:i:s");
            //creat the trip and link the start address to the trip
            $cltData = array(
                'datePickUp'  => $date,
                'destination_address' => $destinationAddressData[0]->addressID,
                'taxi' => $user[0]->userID
            );
            $where = array('tripId' => $tripID);
            $this->_trip_model = new \models\trip_model();
            $this->_trip_model->setDestination($cltData, $where);

            $this->_user_model = new \models\user_model();
            $this->_user_model->setStatus(session::get('user')->userID,0);

            $trip = $this->_trip_model->getTrip($tripID);
            $data['trip'] = $trip[0];
        } else {
            $data['trip'] = null;
        }
        $this->getPath($trip[0]->start_address, $trip[0]->destination_address, $trip[0]->tripId);
    }

    public function getPath($start_address, $destination_address, $tripID)
    {

        $data['lbltitle'] = $this->language->get('title_trip_distination');
        $data['lblclt_name'] = $this->language->get('clt_name');
        $data['lblclt_Phone'] = $this->language->get('clt_Phone');
        $data['lblstarting_adress'] = $this->language->get('Address de destination');
        $data['lblpostal_code'] = $this->language->get('Code postal');
        $data['lblcity'] = $this->language->get('Ville');

        $this->_address_model = new \models\Address_model();

        $data['$start_address'] = $this->_address_model->getAddress($start_address);
        $data['$destination_address'] = $this->_address_model->getAddress($destination_address);
        $data['tripID'] = $tripID;
        View::rendertemplate('header', $data);
        View::render('taxidriver/destinationPath', $data);
        View::rendertemplate('footer', $data);
    }

    public function endTrip($tripID,$duration,$distance){

        //creat the trip and link the start address to the trip

        $cltData = array(
            'tripTime' => $duration,
            'tripLength' => $distance,
        );
        $where = array('tripId' => $tripID);
        $this->_trip_model = new \models\trip_model();
        $this->_trip_model->updateTrip($cltData, $where);

        $this->_user_model = new \models\user_model();
        $this->_user_model->setStatus(session::get('user')->userID,1);   // set status to availible

        url::redirect('taxi');

    }
}