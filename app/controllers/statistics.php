<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 31/03/15
 * Time: 3:09 PM
 */

namespace controllers;
use core\view,
    \helpers\session,
    \helpers\password,
    helpers\an_paginator,
    \helpers\url;
use models\trip_model;

class statistics extends \core\controller
{
    private $_trip_model;
    public function __construct()
    {
        parent::__construct();
        //if not logged in , go to login page
        if (session::get('logedIn')!=true){
            url::redirect('login');
        }
    }

    public function index()
    {
        $this->language->load('agent');
        $data['lbltitle'] = $this->language->get('title');

        $this->_trip_model = new \models\trip_model();
        $data['tripsStatistics'] = $this->_trip_model->getTripsByTaxi();
        View::rendertemplate('header', $data);
        //echo "string";
        View::render('agent/stat', $data);
        View::rendertemplate('footer', $data);
    }
} 