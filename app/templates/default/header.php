<!DOCTYPE html>
<html lang="<?php echo LANGUAGE_CODE; ?>">
<head>

	<!-- Site meta -->
	<meta charset="utf-8">
	<title><?php echo $data['title'].' - '.SITETITLE; //SITETITLE defined in app/core/config.php ?></title>

	<!-- CSS -->
	<?php
		helpers\assets::css(array(
			'//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
			helpers\url::template_path() . 'css/style.css',
		))
	?>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    <script src="<?php echo DIR; ?>app/templates/default/js/jquery.js"></script>

</head>
<body>

<div id="body_container" class="container">
