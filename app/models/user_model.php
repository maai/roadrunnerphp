<?php namespace models;

class User_model extends \core\model {
    private $userDbTab = 'users' ;
    
    public function __construct() {
        parent::__construct();
    }

    //
    private function encriptPw($password) {
        //Todo :encrptage
        //$password = hash('sha256', $password); // On hash le mot de passe en sha256
        return $password;
    }

    public function getUser($login, $password) {
       /* $req="SELECT * FROM users WHERE userLogin =  ".$login." AND userPW= ".$password.";";
        return $req; //$this->_db->select();*/
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->userDbTab ." WHERE userLogin = :userLogin AND userPW= :password;"
            , array(':userLogin' => "$login", ':password' =>  $this->encriptPw($password)));
    }

    public function getTaxiListCount() {
        $data = $this->_db->select("SELECT count(*) as nbr FROM " . PREFIX . $this->userDbTab ."  WHERE userType = :userType; "
            , array(':userType' => "2"));
        return $data[0]->nbr;
    }

    public function getTaxiList($Limit=null) {
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->userDbTab ."  WHERE userType = :userType $Limit; "
            , array(':userType' => "2"));
    }

    public function getActiveTaxis($status,$Limit=null) {
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->userDbTab ." WHERE status = :status $Limit"
            , array(':status' => "$status"));
    }

    //get the position(lat,lng) of a particular taxi
    public function getTaxiPosition($id) {
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->userDbTab ." WHERE userID = :userID "
            , array(':userID' => "$id"));
    }

    public function getUserRol($userLogin) {
        $data = $this->_db->select("select userType from " . PREFIX . $this->userDbTab ." where userLogin = :userLogin", array(':userLogin' => $userLogin));
        return $data[0]->userType;
    }

    public function setStatus($tripID,$status){
        $data = array(
             'status' => $status
        );
        $where = array('userID' => $tripID);
        $this->_db->update(PREFIX.$this->userDbTab,$data, $where);
    }

/*    public function insertUser($user) {

        $user['mdp'] = hash('sha256', $user['mdp']); // On hash le mot de passe en sha256
        $this->_db->insert(PREFIX.'utilisateur',$user);
        return $this->_db->lastInsertId('id');

    }

    public function update_user($data, $where){
        echo ('<BR>----- req------<BR>
                    nom : '          .$data['nom'].'<br>'
            .'prenom : '        .$data['prenom'].'<br>'
            .'mdp : '           .$data['mdp'].'<br>'
            .'email : '         .$data['email'].'<br>'
            .'telephone : '     .$data['telephone'].'<br>'
            .'adresse : '       .$data['adresse'].'<br>'
            .'datenaissance : ' .$data['datenaissance']
            .'where id : ' . $where['id']);
//--------
        $data['mdp'] = hash('sha256', $data['mdp']); // On hash le mot de passe en sha256
        $this->_db->update(PREFIX.$this->userDbTab,$data, $where);
//--------
    }
    */
}
