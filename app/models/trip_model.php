<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 25/03/15
 * Time: 10:05 PM
 */

namespace models;


class trip_model  extends \core\model {
    private $tripDbTab = 'trip' ;

    public function __construct() {
        parent::__construct();
    }



    public function getTripsByTaxi() {
        return $this->_db->select("SELECT tripId , count(tripId) as totalTrips,
                                    sum(tripLength) as totalLength,
                                    avg(TO_SECONDS(datePickup) - TO_SECONDS(dateOrder)) as avgTimePickup ,
                                    avg(TO_SECONDS(dateDelevery) - TO_SECONDS(datePickup)) as avgTimePickup
                                    FROM trip ",  array('tripId' => '*' ));
    }

    // creat new trip from data passed using an array of strings
    public function insertNewTrip($data) {

        $this->_db->insert(PREFIX.'trip',$data);
        return $this->_db->lastInsertId('id');

    }

    public function updateTrip($data, $idTrip) {
        $this->_db->update(PREFIX.$this->tripDbTab,$data, $idTrip);
    }

    public function cancelTrip($idTrip) {

    }

    public function getTrip($idTrip) {
        return $this->_db->select("SELECT *
                                    FROM trip, address, cities
                                    WHERE cities.cityID = address.city
                                    AND address.addressID = trip.start_address
                                    AND trip.tripId = :tripId;"
            , array(':tripId' => "$idTrip"));
    }


    /** Returns list address located within  x Km*/
    public function selectAddressWithin($lat,$lng,$distance,$limit=null){
        //get clt info : Id, name,  address , cp , phone, lat, lmg

        return $this->_db->select("SELECT trip.tripId , trip.clt_name, trip.clt_Phone_Number,address.addressLine,address.postalcode,address.lat,address.lng, "
            ."( 6371 * acos( cos( radians(:center_lat) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(:center_lng) ) + sin( radians(:center_lat) ) * sin( radians( lat ) ) ) ) AS distance"
            ."  FROM trip , address where (address.addressID = trip.start_address and trip.destination_address IS NULL ) HAVING distance < :radius ORDER BY distance $limit;"
            , array(  ':center_lat' => $lat,
            ':center_lng' => $lng,
            ':center_lat' => $lat,
            ':radius' => $distance
        ));
    }

    public function setDestination($data, $where){
       $this->_db->update(PREFIX."trip",$data, $where);
    }
}