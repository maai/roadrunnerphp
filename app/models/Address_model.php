<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 26/03/15
 * Time: 8:52 PM
 */

namespace models;


class Address_model   extends \core\model {

    private $addressDbTab = 'address' ;
    private $countrieslistDbTab = 'countrieslist' ;
    private $provincesDbTab = 'provinces' ;
    private $citiesDbTab = 'cities' ;

    public function __construct() {
        parent::__construct();
    }

    public function getContrysList() {
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->countrieslistDbTab .";");
    }

    public function getProvincesListByContry($idContry) {
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->provincesDbTab ." WHERE provinceID = :provinces_country;"
            , array(':provinces_country' => "$idContry"));

    }

    public function getCitiesListByProvinces($idProvinces) {
        return $this->_db->select("select * from " . PREFIX . $this->citiesDbTab ." where city_Province = :city_Province"
            , array(':city_Province' => $idProvinces));
    }

    public function getAddress($idAddress) {
        return $this->_db->select("SELECT * FROM " . PREFIX . $this->addressDbTab ." where addressID = :address_ID;"
            , array(':address_ID' => $idAddress));
    }

    public function setAddress($idContry=null,$Province=null, $idCitie) {

    }

    public function insertNewAddress($data) {
        $this->_db->insert(PREFIX.$this->addressDbTab,$data);
        return $this->_db->lastInsertId('addressID');
    }

    public function updateAddress($data,$where) {
        $this->_db->update(PREFIX.$this->userDbTab,$data, $where);
    }


    /* geolocalisation section */
    /** Returns list address located within  x Km*/
    public function selectAddressWithin($lat,$lng,$distance){
        //get clt info : Id, name,  address , cp , phone, lat, lmg
        $req="SELECT 'tripId'.tripId, 'tripId'.clt_name, 'tripId'.clt_Phone_Number,'address'.addressLine,'address'.postalcode,'address'.lat,'address'.lng,
              ( 6371 * acos( cos( radians(':center_lat') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(':center_lng') ) + sin( radians(':center_lat') ) * sin( radians( lat ) ) ) ) AS distance
                FROM trip , address where ('address'.addressID = 'tripId'.start_address) HAVING distance < ':radius' ORDER BY distance LIMIT 0 , 20;";

        return $this->_db->select($req, array(  ':center_lat' => $lat,
                                        ':center_lng' => $lng,
                                        ':center_lat' => $lat,
                                        ':radius' => $distance
                                ));
    }
}