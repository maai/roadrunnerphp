<?php
$clients=$data['tripList'];
$lat=$data['tacxiLat'];
$lng=$data['tacxiLng'];

?>

<div class="panel panel-default" style="margin: 0px;">
    <div class="panel-heading">
        <h3 class="panel-title"><?= count($data['tripList']); ?> cliens disponible</h3>
    </div>
    <div class="panel-body">
        <div class="list-group" style="margin: 0px;">

            <div class='list-group-item'>
                <?php foreach($clients as $clt){
                    $formatedAddress=$clt->addressLine.",". $clt->city."". $clt->postalcode;
                    $distanceXmlUrl = "https://maps.googleapis.com/maps/api/distancematrix/xml?origins=45.4918655,-73.5661408&destinations=$formatedAddress&key=".GOOGLE_KEY;

                    $xml=simplexml_load_file($distanceXmlUrl) or die("Error: Cannot create object");  ?>
                    <a href="<?= DIR."selectTrip/".$clt->tripId ?>" class="list-group-item">
                        <h5  class="list-group-item-heading"> <b>distacce:</b> <?= $xml->row->element->distance->text ?>, joiniable en :<?= $xml->row->element->duration->text ?></h5>
                        <p class="list-group-item-text"><?= $formatedAddress?></p>
                    </a>
                <?php } ?>
            </div>
            <div><?= $data['page_links']?></div>
        </div>
    </div>
</div>
