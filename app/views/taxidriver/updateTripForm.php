<?php
$trip =$data['trip'];
$address=$data['address'];
?>
<div class="container" style="margin-top: 100px ;">
    <?= $error ?>

    <div  class="panel panel-primary container login-box" style="width:400px; top: 100px" >
        <form role="form" class="form-horizontal" action="<?php echo DIR; ?>saveTrip"
              method="POST">
            <fieldset>
                <input type="hidden" id="tripID" name="tripID" value="<?php echo ($trip[0]->tripId) ?>">
                <!-- Form Name -->
                <legend><?php echo $data['lbltitle'] ?> </legend>
                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="txtCltName"><?php echo ($data['lblclt_name']." : ".$trip[0]->clt_name) ?> </label>
                </div>
                <!-- Phone input-->
                <div class="control-group">
                    <label class="control-label" for="txtCltPhone"><?php echo ($data['lblclt_Phone']." : ".$trip[0]->clt_Phone_Number) ?></label>
                </div>

                <!-- textarea starting_adress input-->
                <div class="control-group">
                    <label class="control-label" for="DestinationAdress"><?php echo $data['lblstarting_adress'] ?></label>
                    <div class="controls">
                        <textarea id="DestinationAdress" name="DestinationAdress" class="form-control" rows="3" placeholder="<?= $data['lblstarting_adress'] ?>"></textarea>
                    </div>
                </div>

                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="txtCP"><?php echo $data['lblpostal_code'] ?></label>
                    <div class="controls">
                        <input id="txtCP" name="txtCP" placeholder="<?= $data['lblpostal_code'] ?>"
                               class="input-xlarge form-control" required="" type="text">
                    </div>
                </div>

                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="cityList"><?php echo $data['lblcity'] ?></label>
                    <div class="controls">
                        <select class="form-control" class="input-xlarge form-control" required="" type="text" id="cityList" name="cityList" >
                            <option value= 'null' > -- choisire la vile -- </option>"
                            <?php
                            foreach ($data['cityList'] as $city){
                                echo "<option value= '".$city->cityID."' > $city->cityName </option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!-- Button -->
                <div class="control-group">
                    <label class="control-label" for="valider"></label>
                    <div class="controls" style="padding: 5px">
                        <button id="btNewTrip" name="submit" class="btn btn-primary"><?=  $data['bt_submit'] ?></button>
                        <input type="reset" class="btn btn-primary" value="<?=  $data['bt_rest'] ?>">
                        <button id="btCancel" name="submit" class="btn btn-primary"><?=  $data['bt_cancel'] ?></button>

                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <script>
        $(document).ready(function() {
            /*$("#btNewTrip").click(function(){
             $.post("demo_test_post.asp",
             {
             txtCltName:  $("#txtCltName").value,
             txtCltPhone:  $("#txtCltName").value,
             txtStartingAdress:  $("#txtCltName").value,
             txtCP:  $("#txtCltName").value,
             cityList:  $("#txtCltName").value

             },
             function(data, status){
             $("#window-canvas").fadeToggle();
             });
             });*/

            $("#btCancel").click(function(){
                $("#window-canvas").fadeToggle();
            });



        });
    </script>
</div>