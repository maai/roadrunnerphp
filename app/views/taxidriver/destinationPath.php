<?php
$start_address = $data['$start_address'];
$destination_address = $data['$destination_address'];
$xmlUrl="https://maps.googleapis.com/maps/api/directions/xml?origin=".$start_address[0]->addressLine.",".$start_address[0]->city.",QC,".$start_address[0]->postalcode."&destination=".$destination_address[0]->addressLine.",".$destination_address[0]->city.",QC,".$destination_address[0]->postalcode."&language=fr&key=".GOOGLE_KEY;
$pathXml=simplexml_load_file($xmlUrl) or die("Error: Cannot create object");
?>
<div class="panel panel-default" style="margin: 5px; max-width: 600px;">
<!-- head Data -->
<div id="head-Data" class="panel-heading ">     <p>Directions : </p> </div>
<div id="trajectory steps" class="panel-body">
    <p>
        <span id="end_address">
            en dirrection de  :  <span id="end-address-text"><?= $pathXml->route->leg->end_address  ?></span>
        </span>
        <span id="distance">
            distance :           <span id="distance-text"><?= $pathXml->route->leg->distance->text  ?></span>
        </span>
    </p>
    <span id="duration">
        atteinds distination en :  <span id="duration-text"><?= $pathXml->route->leg->duration->text  ?></span>
    </span>
</div>
<!-- End head Data -->
<!-- trajectory steps -->
<?php
$steps = $pathXml->route->leg->step;
foreach ( $pathXml->route->leg->step as $step ) {?>
    <div id="trajectory steps" class="list-group-item" >
        <p><span id="duration-text"><?= $step->duration->html_instructions  ?></span></p>
        <span id="duration-text"><?= $step->html_instructions  ?></span>
    </div>
<?php } ?>
</div>

<!-- Button -->
<div class="control-group">
    <div class="controls" style="padding: 5px; ">
        <a href="<?php echo DIR; ?>endTrip/<?=  $data['tripID']  ?>/<?= $pathXml->route->leg->duration->value  ?>/<?= $pathXml->route->leg->distance->value  ?>" class="btn btn-default btn-block"  role="button" style="width: 70px; "> Arriver </a>
    </div>
</div>


<!-- End trajectory steps -->
<span id="copyrights" ><?= $pathXml->route->copyrights  ?></span>
