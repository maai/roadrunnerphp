<?php
/**
 * Created by PhpStorm.
 * User: anas
 * Date: 06/03/15
 * Time: 11:27 AM
 */ ?>
<div id="taxi-navigation-menu-canvas" class="well center-block">
    <div>
        <a id="bt_refresh"  href="#" class="btn btn-default btn-block"  role="button" >Refrech</a>
        <a href="<?= DIR."logout" ?>" class="btn btn-default btn-block"  role="button" >Quitter</a>
    </div>
    <div id="trip-list-canvas"></div>
</div>
<div id="map-canvas"></div>
<div id="window-canvas" style="position: absolute; top: 0px; left 0px; padding: 10px 0px; z-index: 15; margin:auto; width:100%; higth:100%" >
</div>


<script>
    function initialize() {

        var myLatlng = new google.maps.LatLng( 45.5016889,-73.567256);
        var mapOptions = {
            zoom: 15,
            center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });
        // Todo : ajouter un tableau de markers pour designier l'emplacement des taxi sur la map

    }

    google.maps.event.addDomListener(window, 'load', initialize);

    var x = document.getElementById("demo");
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPositionTest);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }
    function showPositionTest(position) {
        $("#trip-list-canvas").load("<?php echo DIR; ?>taxi/availableTrips/45.4918655/-73.5661408");
    }

    function showPosition1(position) {
        $("#trip-list-canvas").load("<?php echo DIR; ?>taxi/availableTrips/" + position.coords.latitude + "/" + position.coords.longitude);
    }

   /* var myVar = setInterval(function(){ getLocation() }, 10000);
    function myTimer() {
        var d = new Date();
        var t = d.toLocaleTimeString();
        $("#trip-list-canvas").load("<?php echo DIR; ?>agent/newtrip");
    }*/

    $(document).ready(function() {
     /*
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPositionTest);
            } else {
                $("#trip-list-canvas").innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function showPositionTest(position) {
            $("#trip-list-canvas").load("<?php echo DIR; ?>taxi/availableTrips/45.4918655/-73.5661408");
        }

        function showPosition1(position) {
            $("#trip-list-canvas").load("<?php echo DIR; ?>taxi/availableTrips/" + position.coords.latitude + "/" + position.coords.longitude);
        }*/

        $("#bt_refresh").click(function(){
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPositionTest);
            } else {
                $("#trip-list-canvas").innerHTML = "Geolocation is not supported by this browser.";
            }
        });

        $("#btCancel").click(function(){
            $("#window-canvas").fadeToggle();
        });

        function showPositionTest(position) {
            $("#trip-list-canvas").load("<?php echo DIR; ?>taxi/availableTrips/45.4918655/-73.5661408");
        }

    });

</script>
