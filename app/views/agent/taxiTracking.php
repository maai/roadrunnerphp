<div class="container" style="margin-top: 100px ;">
    <?= $error ?>
    <legend><?php echo $data['lbltitle'] ?> </legend>

    <table class="table table-striped table-condensed col-sm-12">
        <thead> <tr>
            <td class="col-sm-2">ID</td>
            <td class="col-sm-2"><?php echo $data['lblclt_name']; ?></td>
            <td class="col-sm-2"><?php echo $data['lblclt_Latitude']; ?></td>
            <td class="col-sm-2"><?php echo $data['lblclt_Longitude']; ?></td>

        </tr>
        </thead> <tbody>
        <?php
        foreach($data['users'] as $User) {
            echo ("
                <tr>
                    <td>$User->userID</td>
                    <td>$User->userFirstName  $User->userLastName</td>
                    <td>$User->lat</td>
                    <td>$User->lng</td>
                </tr>
            ");
        }
        ?>
        </tbody>
            <tfoot>
            <tr>
                <td colspan="6"><?=$data['page_links']?></td>
            </tr>
            <tr>

            </tr>
        </tfoot>
    </table>

</div>
<div class="container">
    <div class="control-group">
        <div class="controls" style="padding: 5px; ">
            <!-- menu option 2 : taxi tracking -->
            <a href="<?php echo DIR; ?>agent" class="btn btn-default btn-block"  role="button" style="width: 70px; ">OK</a>
        </div>
    </div>
</div>