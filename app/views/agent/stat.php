<div id="stat-canva " >
<?php foreach($data['tripsStatistics'] as $tripStat){ ?>
    <div id="div-taxi-<?= $tripStat->tripId  ?>" class="panel panel-default" style="margin: 5px; max-width: 600px;">
        <div id="trajectory steps" class="list-group-item"  >
            Nombre totale des trajet : <span id="duration-text"><?= $tripStat->totalTrips  ?></span>
        </div>
        <div class="list-group-item"  >
            Durée totale des trajet <span id="duration-text"><?= gmdate("H:i:s",$tripStat->totalLength)  ?></span>
        </div>
        <div class="list-group-item"  >
            Durée moyenne des trajet de recuperation : <span id="duration-text"><?= gmdate("H:i:s",$tripStat->avgTimePickup)  ?></span>
        </div>
        <div class="list-group-item"  >
            Durée moyenne des trajet de livraison : <span id="duration-text"><?= gmdate("H:i:s", $tripStat->avgTimePickup) ?></span>
        </div>
    </div>
<?php } ?>
</div>