<div id="navigation-menu-canvas" class="well center-block">

    <!-- menu option 1 : add a new trip -->
    <a id="btAddNewTrip" href="#" class="btn btn-default btn-block"  role="button" style=" height:90px; width: 90px; margin: 5px; padding: 5px ;">Nouvel <br />cource</a>
    <!-- menu option 2 : taxi tracking -->
    <a href="<?php echo DIR; ?>agent/taxiTracking" class="btn btn-default btn-block"  role="button" style=" height:90px; width: 90px; margin: 5px; padding: 5px ;">Suivie <br />de taxis</a>
    <!-- menu option 3 : statistics -->
    <a  href="<?php echo DIR; ?>stat" class="btn btn-default btn-block"  role="button" style=" height:90px; width: 90px; margin: 5px; padding: 5px ;">Stastiques</a>
    <!-- menu option 4 : logout -->
    <a  href="<?php echo DIR; ?>logout" class="btn btn-default btn-block"  role="button" style=" height:90px; width: 90px; margin: 5px; padding: 5px ;margin-top: 100px";">Quitter</a>

</div>

<div id="map-canvas"></div>
<div id="window-canvas" style="position: absolute; top: 0px; left 0px; padding: 10px 0px; z-index: 15; margin:auto; width:100%; higth:100%" >
</div>
<script>
    function initialize() {

//create array to store a set of location
        var collectionTaxi = new Array();
        var collectionClient = new Array();

//a set of locations stored in array
        collectionTaxi[0] = new google.maps.LatLng(45.495989, -73.573276);
        collectionTaxi[1] = new google.maps.LatLng(45.507268, -73.563213);
        collectionTaxi[2] = new google.maps.LatLng(45.492199, -73.562489);

        collectionClient[0] = new google.maps.LatLng(45.493215, -73.557232);
        collectionClient[1] = new google.maps.LatLng(45.498246, -73.571364);
        collectionClient[2] = new google.maps.LatLng(45.496160, -73.571457);


        var myLatlng = new google.maps.LatLng( 45.5016889,-73.567256);

        var mapOptions = {
            zoom: 15,
            center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'Hello World!'
        });

//create number of markers based on collection.length
        var pointMarkerTaxi = new Array();//store marker in array
        function setPointTaxi(){
            for(var i=0; i<collectionTaxi.length; i++){

                var icon = new google.maps.MarkerImage('http://samuelbellerose.com/images/Taxi_icon.png');
                pointMarkerTaxi[i] = new google.maps.Marker({
                    position: collectionTaxi[i],
                    map: map,
                    icon: icon
                });
            }
        }

        var pointMarkerClient = new Array();//store marker in array
        function setPointClient(){
            for(var i=0; i<collectionClient.length; i++){

                var icon = new google.maps.MarkerImage('http://samuelbellerose.com/images/Client_icon.png');
                pointMarkerClient[i] = new google.maps.Marker({
                    position: collectionClient[i],
                    map: map,
                    icon: icon
                });
            }
        }

        setPointTaxi();
        setPointClient();
        // Todo : ajouter un tableau de markers pour designier l'emplacement des taxi sur la map

    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $(document).ready(function() {

        $("#btAddNewTrip").click(function(){
            $("#window-canvas").show("slow");
            $("#window-canvas").load("<?php echo DIR; ?>agent/newtrip");

        });

        $("#btCancel").click(function(){
            $("#window-canvas").fadeToggle();
        });


    });

</script>

