<?php
use \helpers\form,
    \core\error;
    $error=null;
    if ($data['error']!=null){
        $error=Error::display($data['error']);
    }
?>

<div style="margin-top: 100px ;">
    <?= $error ?>

    <div  class="panel panel-primary container login-box" style="width:300px; top: 100px" >
        <form role="form" class="form-horizontal" action="<?= DIR?>login"
              method="POST">
            <fieldset>
                <!-- Form Name -->
                <legend>Login</legend>
                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="login"><?php echo $data['login'] ?></label>
                    <div class="controls">
                        <input id="login" name="login" placeholder="<?= $data['login'] ?>"
                               class="input-xlarge form-control" required="" type="text">
                    </div>
                </div>
                <!-- Password input-->
                <div class="control-group">
                    <label class="control-label" for="password"><?= $data['password'] ?> </label>
                    <div class="controls">
                        <input id="password" name="password" placeholder="<?= $data['password'] ?>"
                               class="input-xlarge form-control" required="" type="password">
                    </div>
                </div>
                <!-- Button -->
                <div class="control-group">
                    <label class="control-label" for="valider"></label>
                    <div class="controls" style="padding: 5px">
                        <button id="submit" name="submit" class="btn btn-primary"><?=  $data['bt_submit'] ?></button>
                        <input type="reset" class="btn btn-primary" value="<?=  $data['bt_rest'] ?>">
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>