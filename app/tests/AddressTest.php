<?php
/**
 * Created by PhpStorm.
 * User: samuelbellerose
 * Date: 15-03-31
 * Time: 01:13
 */
require_once 'app/models/Address_model.php';

class AddressTest extends PHPUnit_Framework_TestCase {



    public function testInsertNewAdressInDatabase() {

        $adressModel = new Address_model();

        $initialAdressCount = $adressModel->_db->select("SELECT COUNT(*) FROM " . PREFIX . $adressModel->addressDbTab );


        $this->assertEquals(4, $initialAdressCount);
    }

}