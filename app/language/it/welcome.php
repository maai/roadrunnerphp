<?php

return array(

	// Index method
	'welcome_text' => 'Benvenuto',
	'welcome_message' => '
		Ciao, benvenuto dal controller di benvenuto! <br/>
		Questo contenuto può essere cambiato in <code>/app/views/agent/login.php</code>
	',

	// Subpage method
	'subpage_text' => 'Sottopagina',
	'subpage_message' => '
		Ciao, benvenuto dal controller di benvenuto e dal metodo sottopagina! <br/>
		Questo contenuto può essere cambiato in <code>/app/views/agent/connectionError.php</code>
	',

	// Buttons
	'open_subpage' => 'Apri sottopagina',
	'back_home' => 'Home',

);
