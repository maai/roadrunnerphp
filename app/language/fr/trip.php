<?php

return array(

    // Form

    'clt_name'          => 'Nom client',
    'clt_Phone'          => 'telphone client',
    'starting_adress'   => 'Address de depart',
    'postal_code'       => 'Code postal',
    'city'              => 'Ville',
    'contry'            => 'Pays',
    'province'          => 'Province',


    //Titles
    'title' => 'Nouvel course',
    'titleConnectionError'  => 'Erreur de connection',
    'title_detail_trip'     => 'detaile de la course',
    'title_trip_distination'     => 'Destintion',


    // Errors
    'connection_error_message' => "
        une erreur c\'est produite veuillez contacter votre administarateur <br/>
	",


    // Buttons
    'bt_submit' => 'Enregistrer',
    'bt_rest'    => 'Réinitialiser',
    'back_home'  => 'Accueil',
    'bt_cancel' => 'Anuller',

);