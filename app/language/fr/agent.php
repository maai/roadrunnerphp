<?php

return array(

    // Index method
    'title' => 'Maine',
    'title_taxi_list'=>'Liste des taxis',



    'clt_name'          => 'Nom client',
    'clt_Phone'          => 'telphone client',
    'starting_adress'   => 'Address de depart',
    'postal_code'       => 'Code postal',
    'city'              => 'Ville',
    'date'              => 'Date',
    'latitude'          => 'latitude',
    'longitude'         => 'longitude',

    'next'              => 'Suivant',
    'previous'          => 'Précédant',




    // Buttons
    'bt_submit' => 'Enregistrer',
    'bt_rest'   => 'Réinitialiser',
    'back_home' => 'Accueil',
    'bt_cancel' => 'Anuller'





);