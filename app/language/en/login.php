<?php
/**
 * Created by PhpStorm.
 * User: Anass Maai
 * Date: 05/03/15
 * Time: 11:09 PM
 */

return array(

    //Title
    'title' => 'Page de connection',
    'titleConnectionError' => 'Erreur de connection',

    // Login Form
    'login'    => 'Login',
    'password' => 'Mot de passe',

    // Errors
    'connection_error_message' => '
        echec de connection<br/>
		Veuillez verifier votre Login et Mot de passe.<br/>
	',


    // Buttons
    'bt_Connect' => 'Connection',
    'bt_rest'    => 'Réinitialiser',
    'back_home'  => 'Accueil',

);