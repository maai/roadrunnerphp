<?php

return array(

	// Index method
	'welcome_text' => 'Welcome',
	'welcome_message' => '
		Hello, agent from the agent controller! <br/>
		This content can be changed in <code>/app/views/agent/login.php</code>
	',

	// Subpage method
	'subpage_text' => 'Subpage',
	'subpage_message' => '
		Hello, agent from the agent controller and subpage method! <br/>
		This content can be changed in <code>/app/views/agent/connectionError.php</code>
	',

	// Buttons
	'open_subpage' => 'Open subpage',
	'back_home' => 'Home',

);