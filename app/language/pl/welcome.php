<?php

return array(

	// Welcome method
	'welcome_text' => 'Witaj',
	'welcome_message' => '
		Cześć, witaj z kontrolera agent! <br/>
		Tą treść możesz zmienić w pliku <code>/app/views/agent/login.php</code>
	',

	// Subpage method
	'subpage_text' => 'Podstrona',
	'subpage_message' => '
		Cześć, witaj z kontrolera agent i metody subpage! <br/>
		Tą treść możesz zmienić w pliku <code>/app/views/agent/connectionError.php</code>
	',

	// Buttons
	'open_subpage' => 'Otwórz podstronę',
	'back_home' => 'Strona główna',

);
