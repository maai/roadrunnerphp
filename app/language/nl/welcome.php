<?php

return array(

	// Index method
	'welcome_text' => 'Welkom',
	'welcome_message' => '
		Hallo, welkom van de agent controller! <br/>
		Deze content kan in worden veranderd <code>/app/views/agent/login.php</code>
	',

	// Subpage method
	'subpage_text' => 'Subpagina',
	'subpage_message' => '
		Hallo, welkom van de agent controller en methoden subpagina! <br/>
		Deze content kan in worden veranderd <code>/app/views/agent/connectionError.php</code>
	',

	// Buttons
	'open_subpage' => 'Open subpagina',
	'back_home' => 'Home',

);