<?php
if(file_exists('vendor/autoload.php')){
	require 'vendor/autoload.php';
} else {
	echo "<h1>Please install via composer.json</h1>";
	echo "<p>Install Composer instructions: <a href='https://getcomposer.org/doc/00-intro.md#globally'>https://getcomposer.org/doc/00-intro.md#globally</a></p>";
	echo "<p>Once composer is installed navigate to the working directory in your terminal/command promt and enter 'composer install'</p>";
	exit;
}

if (!is_readable('app/core/config.php')) {
	die('No config.php found, configure and rename config.example.php to config.php in app/core.');
}

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * You can load different configurations depending on your
 * current environment. Setting the environment also influences
 * things like logging and error reporting.
 *
 * This can be set to anything, but default usage is:
 *
 *     development
 *     production
 *
 * NOTE: If you change these, also change the error_reporting() code below
 *
 */
	define('ENVIRONMENT', 'development');
/*
 *---------------------------------------------------------------
 * ERROR REPORTING
 *---------------------------------------------------------------
 *
 * Different environments will require different levels of error reporting.
 * By default development will show errors but production will hide them.
 */

if (defined('ENVIRONMENT')){

	switch (ENVIRONMENT){
		case 'development':
			error_reporting(E_ALL);
		break;

		case 'production':
			error_reporting(0);
		break;

		default:
			exit('The application environment is not set correctly.');
	}

}

//initiate config
new \core\config();

//create alias for Router
use \core\router,
    \helpers\url;

//define routes
/*
Router::any('', '\controllers\login@index');
Router::any('/subpage', '\controllers\login@subpage');
*/
//Router::get('blog/(:any)', '\controllers\blog@post');


// define routs for login pg
Router::any     ('',                '\controllers\login@index');
Router::any     ('login',          '\controllers\login@login');
Router::any     ('login/(:any)',    '\controllers\login@login');
Router::get     ('logout',          '\controllers\login@logout');

//agents console
Router::any     ('agent',                       '\controllers\agent@index');
Router::any     ('agent/index',                 '\controllers\agent@index');
Router::any     ('agent/newtrip',               '\controllers\agent@newtrip');
Router::any     ('agent/addTrip',        '\controllers\agent@addTrip');
Router::any     ('agent/addTrip/(:any)',        '\controllers\agent@addTrip');


Router::any     ('agent/cancelTrip/(:any)',     '\controllers\agent@cancelTrip');
Router::any     ('agent/updateTrip/(:any)',     '\controllers\agent@updateTrip');
Router::any     ('agent/showTrip/(:any)',       '\controllers\agent@showTrip');
Router::any     ('agent/taxiRtp',               '\controllers\stats@taxiRtp');

// taxi tracking
Router::any     ('agent/taxiTracking',                '\controllers\agent@taxisTracking');
Router::any     ('agent/taxiTracking/(:any)',         '\controllers\agent@taxisTracking');

Router::any     ('agent/getItXml',         '\controllers\agent@taxisTrackingXml');
Router::any     ('agent/getItXml/(:any)',         '\controllers\agent@taxisTrackingXml');
//


//-----> statistics
Router::any     ('stat',                        '\controllers\statistics@index');
	

/** driver console */
/**/
Router::any     ('taxi',                                '\controllers\taxidriver@index');
Router::any     ('taxi/(:any)/(:any)',                  '\controllers\taxidriver@index');
Router::any     ('taxi/index/(:any)/(:any)',            '\controllers\taxidriver@index');
Router::any     ('taxi/availableTrips',                 '\controllers\taxidriver@availableTrips');
Router::any     ('taxi/availableTrips/(:any)/(:any)',   '\controllers\taxidriver@availableTrips');
Router::any     ('taxi/updateTrip/(:any)',              '\controllers\taxidriver@updateATrip');
Router::any     ('taxi/refuseTrip',                     '\controllers\taxidriver@index');
Router::any     ('selectTrip',                          '\controllers\taxidriver@selectTrip');
Router::any     ('selectTrip/(:any)',                   '\controllers\taxidriver@selectTrip');
Router::any     ('updateTrip/(:any)',                  '\controllers\taxidriver@updateTrip');
Router::any     ('updateTrip',                         '\controllers\taxidriver@updateTrip');
Router::any     ('saveTrip',                           '\controllers\taxidriver@saveTrip');
Router::any     ('endTrip/(:any)/(:any)/(:any)',       '\controllers\taxidriver@endTrip');



//if no route found
Router::error('\core\error@index');

//turn on old style routing
Router::$fallback = false;

//execute matched routes
Router::dispatch();
