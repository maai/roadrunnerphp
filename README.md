# README #


### Description ###

Road Runner est un projet visant à automatiser la réservation de taxi et à faciliter la communication entre l’agence et les taxieurs. Une fois mis en place, le système permettra aux agences d’optimiser le temps de réponse à une réservation de taxi d’un client, ainsi que la disponibilité et la qualité du service livré aux clients.

Version 1.0

Documentation (**read only**): [Google drive](https://drive.google.com/folderview?id=0B7MEpHgH3iYARlZ6US1VTE9NcXM&usp=sharing)

##Prérequis

 - Apache Web Server ou équivalent avec support de mod rewrite .
 - PHP 5.3 or supérieur
 - MySQL . 

active moduls requared :
- ssl_module in the Apache modules.
- php_openssl in the PHP extensions.

## Installation
mot de passe de la DB par défaut est 
user : root et le mot de passe est vide, pour modifier ça ouvrez  app/core/config.php et modifier les ligne suivantes a votre besoin

            define('DB_HOST', 'localhost');
            define('DB_NAME', 'roadrunnerdb');
            define('DB_USER', 'root');
            define('DB_PASS', '');
            define('PREFIX', '');

le fichier  .htaccess doit être configurer au besoin